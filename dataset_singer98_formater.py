from pydub import AudioSegment
import os
def m4a2mp3(dir_path='unzip',test=True):
    for root,dirs,names in os.walk(dir_path):
        for name in names:
            file_path=os.path.join(root,name)
            if '.m4a' in file_path:
                top,singer,music=file_path.split(os.sep)
                des_path=os.path.join('singer43',singer,singer,music.replace('.m4a','.mp3'))
                des_dirs=os.path.dirname(des_path)
                print(des_path)
                if os.path.isfile(des_path):
                    continue
                if not test:
                    if not os.path.isdir(des_dirs):
                        os.makedirs(des_dirs)
                    try:
                        song=AudioSegment.from_file(file_path,'m4a')
                        song=song.set_channels(1)
                        song=song.set_frame_rate(16000)
                        song=song.set_sample_width(2)
                        song.export(des_path,'mp3')
                    except:
                        with open('err.log','a') as log_err:
                            log_err.write(file_path+'\t'+des_path+'\n')



    return 0

def err_channel_m4a2mp3():
    with open('err.log', 'r') as log_err:
        log_cont=log_err.readlines()
    for item in log_cont:
        file_path,des_path=item.strip('\n').split('\t')
        song = AudioSegment.from_file(file_path, 'm4a')
        # song = song.set_channels(1)
        song = song.set_frame_rate(16000)
        song = song.set_sample_width(2)
        song.export(des_path, 'mp3')

    return 0
if __name__ == '__main__':
    do_str='12'
    if '1' in do_str:
        m4a2mp3(test=False)
    if '2' in do_str:
        err_channel_m4a2mp3()