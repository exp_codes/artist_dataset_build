import json
import os
import youtube_dl
from tqdm import tqdm
import urllib.parse
import requests
import zipfile
import shutil
def gen_name_pl_json(txt_file):
    # 将手工记录的歌手对应的 YouTube播放列表转换为json文件存储，避免出现重复
    name_pl_dict = {}
    with open(txt_file, 'r') as txt_f:
        txt_cont = txt_f.readlines()
    for item in range(0, len(txt_cont), 3):
        name, num = txt_cont[item].split('#')
        pl = txt_cont[item + 1].strip('\n')
        name_pl_dict[name] = [pl, int(num)]
    print("歌手数量：%s" % len(name_pl_dict.keys()))
    name_pl_str = json.dumps(name_pl_dict, indent=4, ensure_ascii=False)
    json_file = txt_file.replace('.txt', '.json')
    with open(json_file, 'w', ) as j_f:
        j_f.write(name_pl_str)

    return 0


def get_music_video_urls_of_playlist(playlist):
    if not os.path.isfile('playlist/{0}.pl'.format(playlist)):
        cmd = """youtube-dl --flat-playlist --dump-single-json {0} > playlist/{0}.pl
    """.format(playlist)
        print(cmd)
        os.system(cmd)
    else:
        print(playlist)
    return 0


def get_all_urls(youtube_json):
    with open(youtube_json) as you_f:
        you_f_cont = you_f.read()
    json_dict = json.loads(you_f_cont)
    for item in json_dict.keys():
        get_music_video_urls_of_playlist(json_dict[item][0])
    return 0


def get_name_by_value(pl_name, youtube_json):
    value = pl_name.split('.')[0]
    with open(youtube_json) as you_f:
        you_f_cont = you_f.read()
    json_dict = json.loads(you_f_cont)
    for item in json_dict.keys():
        if json_dict[item][0] == value:
            return item


def get_name_list():
    for item in os.listdir('playlist'):
        singer_name = get_name_by_value(item, 'list/70+.json')
        print(singer_name)
        pl_path = os.path.join('playlist', item)
        with open(pl_path) as pl_f:
            pl_cont = pl_f.read()
        pl_dict = json.loads(pl_cont)
        all_urls = []
        for each_url in pl_dict['entries']:
            url = each_url['url']
            print(url)
            all_urls.append(url + '\n')
        with open('namelist/%s' % singer_name, 'w') as sn_f:
            sn_f.writelines(all_urls)
    return 0


def download_one_by_one(nums):
    with open('done')as done_f:
        done_list=done_f.readlines()
    for item in os.listdir('namelist'):
        if item+'\n' in done_list:
            continue

        file_path = os.path.join('namelist', item)
        with open(file_path) as o_f:
            all_urls = o_f.readlines()
        for url in all_urls:
            song_id = url.strip('\n')
            song_path = 'songs/' + item + '/' + song_id + '.m4a'
            print(song_path)
            singer_dir = os.path.dirname(song_path)
            if not os.path.isdir(singer_dir):
                os.makedirs(singer_dir)
            if len(os.listdir(singer_dir)) > nums:
                break
            try:
                with youtube_dl.YoutubeDL() as ydl:
                    dictMeta = ydl.extract_info(
                        "https://www.youtube.com/watch?v={sID}".format(sID=song_id),
                        download=False)
                duration = dictMeta['duration']
                print(duration)

                if duration > 360 or duration < 120:
                    continue
            except:
                continue
            if os.path.isfile(song_path):
                continue
            cmd = "youtube-dl -f 'bestaudio[ext=m4a]' %s --output '%s'" % (song_id, song_path)
            print(cmd)

            os.system(cmd)

    return 0


def download_from_temp_server(url_base='http://giae.iloogn.tools:8989/download/'):
    with open('done')as done_f:
        done_list=done_f.readlines()
    for item in tqdm(done_list):
        name=item.strip('\n')
        url=url_base + urllib.parse.quote(name)
        print(url)
        file_path='zip/%s.zip' % name
        if os.path.isfile(file_path) or os.path.isdir('unzip/%s' % name):
            continue
        myfile = requests.get(url, allow_redirects=True)
        open(file_path, 'wb').write(myfile.content)
    return 0

def unzip_files():
    for item in tqdm(os.listdir('zip')):
        if '.zip' not in item:
            continue
        zip_file_path=os.path.join('zip',item)
        print(zip_file_path)
        des_dir='unzip/%s'%item.strip('.zip')
        if not os.path.isdir(des_dir):
            os.makedirs(des_dir)
        with zipfile.ZipFile(zip_file_path, 'r') as zip_ref:
            zip_ref.extractall(des_dir)
        os.remove(zip_file_path)


    return 0

def count_done(nums=20):
    done_list=[]
    for item in os.listdir('songs'):
        singer_dir=os.path.join('songs',item)
        if not os.path.isdir(singer_dir):
            continue
        if len(os.listdir(singer_dir))>nums-1:
            done_list.append(item+'\n')
    with open('done','a') as done_f:
        done_f.writelines(done_list)
    return 0
def del_done():
    with open('done')as done_f:
        done_list = done_f.readlines()
    for item in done_list:
        item_dir=os.path.join('songs',item.strip('\n'))
        if os.path.isdir(item_dir):
            shutil.rmtree(item_dir)
    return 0
def gen_final_singer_pl(dir_name='singer43',format='mp3'):
    singer_pl={}
    for item in os.listdir(dir_name):
        if not os.path.isdir(dir_name+'/%s/%s'%(item,item)):
            print(dir_name+'/%s/%s'%(item,item))
            #os.remove(dir_name+'/%s/%s'%(item,item))
            continue
        singer_pl[item]=[name.strip('.'+format) for name in os.listdir(dir_name+'/%s/%s'%(item,item)) if '.'+format in name]
    singer_pl_str=json.dumps(singer_pl,indent=4,ensure_ascii=False)
    with open('final_singer_pl.json','w') as fsp_f:
        fsp_f.write(singer_pl_str)
    return 0

def view_all_singer():
    now=[]
    with open('final_singer_pl.json','r') as fsp_f:
        fsp_cont=fsp_f.read()
    fsp_dict=json.loads(fsp_cont)
    print(fsp_dict.keys())
    print(len(fsp_dict.keys()))
    now.extend(fsp_dict.keys())
    expect = []
    with open('list/female.json', 'r') as fsp_f:
        fsp_cont = fsp_f.read()
    fsp_dict = json.loads(fsp_cont)
    print(fsp_dict.keys())
    print(len(fsp_dict.keys()))
    expect.extend(fsp_dict.keys())
    print(sorted(now))
    print(sorted(expect))
    for n,e in zip(sorted(now),sorted(expect)):
        if n!=e:
            print(n+e)
    for item in expect:
        if item not  in now:
            print(item)
    print(len(now))
    print(len(expect))
    with open('list/female.txt', 'r') as fsp_f:
        fsp_cont = fsp_f.readlines()
    name_pl_dict=[]
    for item in range(0, len(fsp_cont), 2):
        name, num = fsp_cont[item].split('#')
        pl = fsp_cont[item + 1].strip('\n')
        name_pl_dict.append(name)
    print(sorted(name_pl_dict))

def count_singer43(num=68):
    all_dir={}
    for root,dirs,names in os.walk('singer43'):
        for name in names:
            fi_path=os.path.join(root,name)
            now_dir=os.path.dirname(fi_path)
            lens_file=len(os.listdir(now_dir))

            if lens_file>num:
                all_dir[now_dir]=lens_file

    print(all_dir)




    return 0
if __name__ == '__main__':
    do_str = '9'#'4','7','56','8'
    if '1' in do_str:#mac运行
        gen_name_pl_json('list/add.txt')
    if '2' in do_str:#临时服务器运行
        get_all_urls('list/70+.json')
    if '3' in do_str:#mac运行
        get_name_list()
    if '4' in do_str:#临时服务器运行
        download_one_by_one(70)
    if '5' in do_str:#GPU计算服务器运行
        download_from_temp_server()
    if '6' in do_str:#GPU计算服务器运行
        unzip_files()
    if '7' in do_str:#完成4后，临时服务器运行
        count_done()
    if '8' in do_str:#完成56后，临时服务器运行
        del_done()
    if '9' in do_str:
        gen_final_singer_pl()
    if '0' in do_str:
        view_all_singer()
    if 'A' in do_str:
        count_singer43(68)
